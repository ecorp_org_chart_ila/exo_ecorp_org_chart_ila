# Exercice ECORP_ORG

## Description database employees
On a une base de données comprenant 300 024 lignes dans la table employees, de 240 124 salariés (```SQL SELECT count(salary) AS nbre FROM salaries WHERE to_date like '9999%';```).
On veut obtenir un organigramme avec l'id, le pid, le titre, le nom de l'employé et le salaire. 

## Requête SQL 
On fait donc une requête SQL pour obtenir ces éléments, puis un programme en python pour transformer la requête en format json.

```SQL
SELECT DISTINCT e.emp_no AS id, concat(first_name," ",last_name) AS name, title, salary AS salary, dm.emp_no AS pid 
FROM employees e
JOIN titles t ON e.emp_no = t.emp_no 
JOIN salaries s ON e.emp_no = s.emp_no 
JOIN dept_emp de ON e.emp_no = de.emp_no
JOIN departments d ON de.dept_no= d.dept_no
JOIN dept_manager dm ON d.dept_no= dm.dept_no 
WHERE de.to_date like '9999%' AND dm.to_date like '9999%' AND t.to_date like '9999%' 
AND s.to_date like '9999%';
```

## Création fichier python pour mettre en relation la database en fichier json
On a créé un fichier main.py en python qui récupère les données de la requête SQL dans un fichier Json, qui est utilisé pour lancer l'organigramme dans une pag web.
Le fichier main.py doit être lancé depuis le dossier ecorp-org/static/.
On obtient le fichier employees.json.


Pense à te donner les droits dans la base de données: 
grant all privileges on employees.* to 'nom'@'localhost';
Remplace nom par nom de la base d'utilisateurs.