import mysql.connector
import json

def recup_et_rec_data_employees():
    try:
        avec_limite = input("Voulez-vous définir une limite? (oui/non): ").lower()
        if avec_limite not in ['oui', 'non']:
            raise ValueError("Réponse invalide. Veuillez répondre par 'oui' ou 'non'.")

        limite = ""
        if avec_limite == 'oui':
            limite = int(input("Entrez la limite pour la requête SQL: "))

    except ValueError as e:
        print(e)
        return

    conn = mysql.connector.connect(
        host="localhost",
        user="alucard",
        password="coucou123",
        database="employees"
    )

    cursor = conn.cursor()

    query = """
    SELECT DISTINCT e.emp_no, CONCAT(e.first_name, " ", e.last_name) AS name, 
    t.title, s.salary AS salary, dm.emp_no AS pid 
    FROM employees e
    JOIN titles t ON e.emp_no = t.emp_no 
    JOIN salaries s ON e.emp_no = s.emp_no 
    JOIN dept_emp de ON e.emp_no = de.emp_no
    JOIN departments d ON de.dept_no = d.dept_no
    JOIN dept_manager dm ON d.dept_no = dm.dept_no 
    WHERE de.to_date LIKE '9999%' AND dm.to_date LIKE '9999%' 
    AND t.to_date LIKE '9999%' AND s.to_date LIKE '9999%'
    """
    
    if avec_limite == 'oui':
        query += f"LIMIT {limite};"

    cursor.execute(query)
    employees = cursor.fetchall()

    cursor.close()
    conn.close()

    employee_data = []
    for emp in employees:
        emp_dict = {
            "id": emp[0],
            "title": emp[2],
            "name": emp[1],
            "salary": float(emp[3])
        }
        if emp[0] != emp[4]:
            emp_dict["pid"] = emp[4]
        employee_data.append(emp_dict)

    with open('./employees.json', 'w') as json_file:
        json.dump(employee_data, json_file, indent=4)


if __name__ == '__main__':
    recup_et_rec_data_employees()