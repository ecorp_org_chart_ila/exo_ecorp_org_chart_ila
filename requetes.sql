USE employees;

-- Sélectionner les noms, numéros, tittres et salaires max des employés
SELECT DISTINCT employees.emp_no, concat(first_name," ",last_name) AS name, title, MAX(salary) FROM employees 
JOIN titles ON employees.emp_no = titles.emp_no 
JOIN salaries ON employees.emp_no = salaries.emp_no GROUP BY emp_no, name, title LIMIT 50;

-- Sélectionner les noms, numéros, tittres et salaires max des employés
SELECT 
        e.first_name,
        e.last_name,
        t.title,
        s.salary,
        d.dept_name,
        CASE 
            WHEN dm.dept_no IS NOT NULL THEN 'Manager'
            ELSE 'Employee'
        END AS grade
    FROM employees e
    JOIN current_dept_emp cde ON e.emp_no = cde.emp_no
    JOIN departments d ON cde.dept_no = d.dept_no
    JOIN titles t ON e.emp_no = t.emp_no AND t.to_date > CURRENT_DATE
    JOIN salaries s ON e.emp_no = s.emp_no AND s.to_date > CURRENT_DATE
    LEFT JOIN dept_manager dm ON e.emp_no = dm.emp_no AND cde.dept_no = dm.dept_no LIMIT 50;

-- Sélectionner les grades
SELECT title, count(title) FROM titles GROUP BY title;


-- Sélectionner les managers en poste
SELECT employees.emp_no, CONCAT(first_name, " ", last_name) FROM employees
JOIN dept_manager ON employees.emp_no = dept_manager.emp_no
WHERE to_date like '9999%';


-- Sélectionner les employés qui ont le plus gros salaire et qui sont managers en 9999
SELECT e.emp_no, CONCAT(first_name, " ", last_name) AS name, MAX(salary) AS salary, dm.to_date
FROM employees e
JOIN dept_manager dm ON e.emp_no = dm.emp_no 
JOIN salaries s ON e.emp_no = s.emp_no
WHERE s.to_date like '9999%'
GROUP BY e.emp_no, name, salary, dm.to_date;

-- Trouver les Pid pour chaque employé
SELECT e.emp_no, dm.emp_no AS pid, dm.dept_no, d.dept_name FROM employees e
JOIN dept_emp de ON e.emp_no=de.emp_no 
JOIN departments d ON de.dept_no= d.dept_no
JOIN dept_manager dm ON d.dept_no= dm.dept_no WHERE de.to_date like '9999%' AND dm.to_date like '9999%';

-- Trouver le salarié le mieux payé
SELECT e.emp_no, e.first_name, e.last_name, s.salary, de.dept_no
FROM employees e
JOIN salaries s ON e.emp_no = s.emp_no
JOIN dept_emp de ON e.emp_no = de.emp_no
ORDER BY s.salary DESC
LIMIT 1;

-- Requête finale pour récupérer l'id, le nom, le titre, le salaire et le pid
SELECT DISTINCT e.emp_no AS id, concat(first_name," ",last_name) AS name, title, salary AS salary, dm.emp_no AS pid 
FROM employees e
JOIN titles t ON e.emp_no = t.emp_no 
JOIN salaries s ON e.emp_no = s.emp_no 
JOIN dept_emp de ON e.emp_no = de.emp_no
JOIN departments d ON de.dept_no= d.dept_no
JOIN dept_manager dm ON d.dept_no= dm.dept_no 
WHERE de.to_date like '9999%' AND dm.to_date like '9999%' AND t.to_date like '9999%' AND s.to_date like '9999%'
LIMIT 50;