WITH RECURSIVE ManagerHierarchy AS (
    SELECT 
        dm.emp_no, 
        e.first_name, 
        e.last_name, 
        dm.dept_no, 
        d.dept_name
    FROM dept_manager dm
    JOIN employees e ON dm.emp_no = e.emp_no
    JOIN departments d ON dm.dept_no = d.dept_no
    WHERE 
        NOT EXISTS (
            SELECT 1 FROM dept_manager dm2 
            WHERE dm2.dept_no = dm.dept_no AND dm2.from_date > dm.from_date
        )
)
SELECT * FROM ManagerHierarchy;


SELECT 
    e.first_name,
    e.last_name,
    t.title,
    s.salary,
    d.dept_name
FROM employees e
JOIN dept_manager dm ON e.emp_no = dm.emp_no
JOIN departments d ON dm.dept_no = d.dept_no
JOIN titles t ON e.emp_no = t.emp_no AND t.to_date = "9999-01-01"
JOIN salaries s ON e.emp_no = s.emp_no AND s.to_date = "9999-01-01";

SELECT     
    de.emp_no,     
    dm.emp_no 
FROM dept_emp AS de 
JOIN departments d ON d.dept_no = de.dept_no AND de.to_date = "9999-01-01"
JOIN dept_manager dm ON dm.dept_no = d.dept_no AND dm.to_date = "9999-01-01"; 

SELECT 
    e.first_name AS prenom,
    e.last_name AS nom,
    t.title AS titre
FROM employees e
JOIN titles t ON e.emp_no = t.emp_no
WHERE t.to_date = '9999-01-01';

SELECT DISTINCT e.emp_no, CONCAT(e.first_name, " ", e.last_name) AS name, 
t.title,s.salary AS salary, dm.emp_no AS pid 
FROM employees e
JOIN titles t ON e.emp_no = t.emp_no 
JOIN salaries s ON e.emp_no = s.emp_no 
JOIN dept_emp de ON e.emp_no = de.emp_no
JOIN departments d ON de.dept_no= d.dept_no
JOIN dept_manager dm ON d.dept_no= dm.dept_no 
WHERE de.to_date LIKE '9999%' AND dm.to_date LIKE '9999%' 
AND t.to_date LIKE '9999%' AND s.to_date LIKE '9999%';